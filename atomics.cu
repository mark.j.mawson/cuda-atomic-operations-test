
#include "cuda_runtime.h"
#include "sm_30_intrinsics.h"
#include "device_launch_parameters.h"
#include <iostream>

#include <stdio.h>
#include <math.h>
//Thrust vectors 
#include <thrust/host_vector.h>
#include <thrust/device_vector.h>
#include <thrust/copy.h>
#include <thrust/device_ptr.h>
#include <thrust/version.h>


/**
 * @name non_Atomic - Read from arrays, adda value to them and write back
 * @param c -  c 
 * @param a -  a 
 * @param b -  b 
 * @param d -  d 
 * @param e -  e 
 * @param f -  f 
 * @param g -  g 
 * @param h -  h 
 * @param ii -  ii 
 * @param j -  j 
 * @param k -  k 
 * @param size -Size of each array (there are 10 arrays) 
 * @return void
 */
template<typename T>
__global__ void non_Atomic(T *c, T *a, T *b, T *d, T *e, T *f, T *g, T *h, T *ii, T *j,T *k,unsigned int size)

{
  int i = blockIdx.x*blockDim.x+threadIdx.x;
  T tempa=1;
  T tempb=2;
  T tempc=3;
  T tempd=4;
  T tempe=5;
  T tempf=6;
  T tempg=7;
  T temph=8;
  T tempi=9;
  T tempj=10;
  if(size>0){
    a[i]=a[i]+tempa;
    b[i]=b[i]+tempb;
    c[i]=c[i]+tempc;
    d[i]=d[i]+tempd;
    e[i]=e[i]+tempe;
    f[i]=f[i]+tempf;
    g[i]=g[i]+tempg;
    h[i]=h[i]+temph;
    ii[i]=ii[i]+tempi;
    j[i]=j[i]+tempj;
  }
  ((tempa+tempb+tempc+tempd+tempe+tempf+tempg+temph+tempi+tempj==0) ? k[i]=tempa+tempb+tempc+tempd+tempe+tempf+tempg+temph+tempi+tempj:temph=tempa+tempb+tempc+tempd+tempe);

}

/**
 * @name atomic_Ind - Simple kernel to write values using independent atomic adds
 * @param c -  c 
 * @param a -  a 
 * @param b -  b 
 * @param d -  d 
 * @param e -  e 
 * @param f -  f 
 * @param g -  g 
 * @param h -  h 
 * @param ii -  ii 
 * @param j -  j 
 * @param k -  k 
 * @param size - Size of each array (there are 10 arrays) 
 * @return void
 */
template<typename T>
__global__ void atomic_Ind(T *c, T *a, T *b, T *d, T *e, T *f, T *g, T *h, T *ii, T *j,T *k,unsigned int size)

{
  int i = blockIdx.x*blockDim.x+threadIdx.x;
  T tempa=1;
  T tempb=2;
  T tempc=3;
  T tempd=4;
  T tempe=5;
  T tempf=6;
  T tempg=7;
  T temph=8;
  T tempi=9;
  T tempj=10;
  if(size>0){
    atomicAdd(&a[i],tempa);
    atomicAdd(&b[i],tempb);
    atomicAdd(&c[i],tempc);
    atomicAdd(&d[i],tempd);
    atomicAdd(&e[i],tempe);
    atomicAdd(&f[i],tempf);
    atomicAdd(&g[i],tempg);
    atomicAdd(&h[i],temph);
    atomicAdd(&ii[i],tempi);
    atomicAdd(&j[i],tempj);
  }
  ((tempa+tempb+tempc+tempd+tempe+tempf+tempg+temph+tempi+tempj==0) ? k[i]=tempa+tempb+tempc+tempd+tempe+tempf+tempg+temph+tempi+tempj:temph=tempa+tempb+tempc+tempd+tempe);

}
/**
 * @name atomic_Shared - Simple kernel to write values using shared atomic adds
 * @param c -  c 
 * @param a -  a 
 * @param b -  b 
 * @param d -  d 
 * @param e -  e 
 * @param f -  f 
 * @param g -  g 
 * @param h -  h 
 * @param ii -  ii 
 * @param j -  j 
 * @param k -  k 
 * @param size -Size of each array (there are 10 arrays) 
 * @return void
 */
template<typename T>
__global__ void atomic_Shared(T *c, T *a, T *b, T *d, T *e, T *f, T *g, T *h, T *ii, T *j,T *k,unsigned int size)

{
  int i = blockIdx.x*blockDim.x+threadIdx.x;
  i=0;
  T tempa=1;
  T tempb=2;
  T tempc=3;
  T tempd=4;
  T tempe=5;
  T tempf=6;
  T tempg=7;
  T temph=8;
  T tempi=9;
  T tempj=10;
  if(size>0){
    atomicAdd(&a[i],tempa);
    atomicAdd(&a[i],tempb);
    atomicAdd(&a[i],tempc);
    atomicAdd(&a[i],tempd);
    atomicAdd(&a[i],tempe);
    atomicAdd(&a[i],tempf);
    atomicAdd(&a[i],tempg);
    atomicAdd(&a[i],temph);
    atomicAdd(&a[i],tempi);
    atomicAdd(&a[i],tempj);

  }
  ((tempa+tempb+tempc+tempd+tempe+tempf+tempg+temph+tempi+tempj==0) ? k[i]=tempa+tempb+tempc+tempd+tempe+tempf+tempg+temph+tempi+tempj:temph=tempa+tempb+tempc+tempd+tempe);

}


/**
 * @name runTests - Set off the various atomic oepration behaviour tests
 * @return cudaError_t
 */
template<typename T, unsigned int size>
cudaError_t runTests()
{
  cudaError_t cudaStatus;




  // initialize random values on host 
  thrust::host_vector<T> b((size-1),1.5);

  b.insert(b.begin()+31,2);

  thrust::host_vector<T> a(size,1);
  thrust::host_vector<T> c(size,0);
  thrust::host_vector<T> d(size,1);
  thrust::host_vector<T> e(size,1);
  thrust::host_vector<T> f(size,1.5);
  thrust::host_vector<T> g(size,1);
  thrust::host_vector<T> h(size,0);
  thrust::host_vector<T> i(size,1);
  thrust::host_vector<T> j(size,1);
  thrust::host_vector<T> k(size,1);


  // copy values to device
  thrust::device_vector<T> d_a = a;
  thrust::device_vector<T> d_b = b;
  thrust::device_vector<T> d_c = c;
  thrust::device_vector<T> d_d = d;
  thrust::device_vector<T> d_e = e;
  thrust::device_vector<T> d_f = f;
  thrust::device_vector<T> d_g = g;
  thrust::device_vector<T> d_h = h;
  thrust::device_vector<T> d_i= i;
  thrust::device_vector<T> d_j = j;
  thrust::device_vector<T> d_k = k;
  // extract raw device pointers
  T *dev_a=thrust::raw_pointer_cast(&d_a[0]);
  T *dev_b=thrust::raw_pointer_cast(&d_b[0]);
  T *dev_c=thrust::raw_pointer_cast(&d_c[0]);
  T *dev_d=thrust::raw_pointer_cast(&d_d[0]);
  T *dev_e=thrust::raw_pointer_cast(&d_e[0]);
  T *dev_f=thrust::raw_pointer_cast(&d_f[0]);
  T *dev_g=thrust::raw_pointer_cast(&d_g[0]);
  T *dev_h=thrust::raw_pointer_cast(&d_h[0]);
  T *dev_i=thrust::raw_pointer_cast(&d_i[0]);
  T *dev_j=thrust::raw_pointer_cast(&d_j[0]);
  T *dev_k=thrust::raw_pointer_cast(&d_k[0]);


  //Declare an array to iterate across the 
  std::vector<unsigned int> CUDABlockSize;
  CUDABlockSize.push_back(32);
  CUDABlockSize.push_back(64);
  CUDABlockSize.push_back(128);
  CUDABlockSize.push_back(256);
  CUDABlockSize.push_back(512);
  CUDABlockSize.push_back(1024);


  //Timing vectors
  std::vector<float> nonAtomic;
  std::vector<float> atomicIndependent;
  std::vector<float> atomicShared;


  //CUDA Timing
  cudaEvent_t start, stop;
  cudaEventCreate(&start);
  cudaEventCreate(&stop);
  float cudaTime;

  for(typename std::vector<unsigned int>::iterator iter = CUDABlockSize.begin(); iter != CUDABlockSize.end(); iter++){


    // Non-atomic independent
    cudaEventRecord(start,0);
    for(int i=1;i<20;i++){
      non_Atomic<<<size/(*iter), *iter>>>(dev_c, dev_a, dev_b,dev_d,dev_e,dev_f, dev_g, dev_h,dev_i,dev_j,dev_k,size);
    }
    cudaEventRecord(stop,0);
    cudaEventSynchronize(stop);
    cudaEventElapsedTime(&cudaTime, start, stop);
    nonAtomic.push_back(cudaTime/20);
    //Independent atomic adds
    cudaEventRecord(start,0);
    for(int i=1;i<20;i++){
      atomic_Ind<<<size/(*iter),*iter>>>(dev_c, dev_a, dev_b,dev_d,dev_e,dev_f, dev_g, dev_h,dev_i,dev_j,dev_k,size);
    }
    cudaEventRecord(stop,0);
    cudaEventSynchronize(stop);
    cudaEventElapsedTime(&cudaTime, start, stop);
    atomicIndependent.push_back(cudaTime/20);
    //Shared atomic adds
    cudaEventRecord(start,0);
    for(int i=1;i<20;i++){
      atomic_Shared<<<size/(*iter),*iter>>>(dev_c, dev_a, dev_b,dev_d,dev_e,dev_f, dev_g, dev_h,dev_i,dev_j,dev_k,size);
    }
    cudaEventRecord(stop,0);
    cudaEventSynchronize(stop);
    cudaEventElapsedTime(&cudaTime, start, stop);
    atomicShared.push_back(cudaTime/20);

  }



  //Print out times

 
  for(std::vector<unsigned int>::size_type i = 0; i != nonAtomic.size(); i++) {
    std::cout<<size<<"\t"<<CUDABlockSize[i]<<"\t"<<size*2*4*10/(nonAtomic[i]/1000.0)<<"\t"<<size*2*4*10/(atomicIndependent[i]/1000.0)<<"\t"<<size*2*4*10/(atomicShared[i]/1000.0)<<std::endl;
  }
  // cudaDeviceSynchronize waits for the kernel to finish, and returns
  // any errors encountered during the launch.
  cudaStatus = cudaDeviceSynchronize();
  if (cudaStatus != cudaSuccess) {
    fprintf(stderr, "cudaDeviceSynchronize returned error code %d after launching addKernel!\n", cudaStatus);
    goto Error;
  }
 Error:


  return cudaStatus;
}


template<unsigned int size>
int master_Functions(){

  master_Functions<size/2>();
  cudaError_t cudaStatus = runTests<float,size>();

  if (cudaStatus != cudaSuccess) {
    fprintf(stderr, "addWithCuda failed!");
    return 1;
  }
  return 0;

}
//end of recursion
template<>
int master_Functions<32>(){
  return 0;
}

int main(){

  std::cout<<"Size"<<"\t"<<"Block Size"<<"Non-atomic"<<"\t"<<"Atomic independent"<<"\t"<<"Atomic shared"<<std::endl;
  master_Functions<8388608>();


  // cudaDeviceReset must be called before exiting in order for profiling and
  // tracing tools such as Nsight and Visual Profiler to show complete traces.
 cudaError_t cudaStatus = cudaDeviceReset();
  if (cudaStatus != cudaSuccess) {
    fprintf(stderr, "cudaDeviceReset failed!");
    return 1;
  }

  return 0;
}
